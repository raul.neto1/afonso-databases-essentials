const mongoose = require('mongoose')

const ClientsSchema = mongoose.Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  birthday: {
    type: String,
    required: true
  },
  gender: {
    type: String
  }
})

module.exports = mongoose.model('Clients', ClientsSchema)