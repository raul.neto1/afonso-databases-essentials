const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const cors = require('cors')
const clientsRoute = require('./routes/clients')

const app = express()

app.use(cors())
app.use(bodyParser.json())

app.all('*', (req, _res, next) => {
  console.info(`method: ${req.method}`)

  return next()
})

app.get('/', (req, res) => {
  res.send('Home')
})

app.use('/clients', clientsRoute)

//In a real project, we would use .env to hide the DB login, but for the purpose of making it easier for anyone testing the application I left it hard coded.
mongoose.connect(
  "mongodb+srv://Afonso:12351235Raul@accluster.k8rsf.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
  () => { console.info('connected to DB') }
)

app.listen(3000)
