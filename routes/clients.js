const express = require('express')
const ClientsModel = require('../models/ClientsModel')

const router = express.Router()

// Return all clients
router.get('/', async (req, res) => {
  console.time('How long to get all')
  ClientsModel.find()
  .then(data => {
    res.json(data)
  })
  .catch(error => {
    console.error(`Error: ${error}`)
    res.json({message: error})
  })
  .finally(() => {
    console.timeEnd('How long to get all')
  })
})

// Add a new client
router.post('/', (req, res) => {
  const client = new ClientsModel({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    birthday: req.body.birthday,
    gender: req.body.gender
  })

  client.save()
    .then(data => {
      console.warn('Expected all fields to have been filled')
      res.json(data)
    })
    .catch(error => {
      console.error(`Error: ${error}`)
      res.json({message: error})
    })
})

// Return a specific client
router.get('/:clientId', (req, res) => {
  console.time('How long to get')
  ClientsModel.findById(req.params.clientId)
    .then(data => {
      res.json(data)
    })
    .catch(({ message }) => {
      console.error(`Error: ${error}`)
      res.json(message)
    })
    .finally(() => {
      console.timeEnd('How long to get')
    })
})

// Delete a specific client
router.delete('/:clientId', (req, res) => {
  ClientsModel.deleteOne({_id: req.params.clientId})
  .then(data => {
    res.json(data)
  })
  .catch(({ message }) => {
    console.error(`Error: ${error}`)
    res.json(message)
  })
})

// Edit a specific client
router.patch('/:clientId', (req, res) => {
  ClientsModel.updateOne(
    { _id: req.params.clientId },
    { $set: {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      birthday: req.body.birthday,
      gender: req.body.gender
    }}
  )
  .then(data => {
    console.warn('Expected all fields to have been filled')
    res.json(data)
  })
  .catch(({ message }) => {
    console.error(`Error: ${error}`)
    res.json(message)
  })
})

module.exports = router